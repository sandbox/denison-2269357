
CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------
Activity Stream twitter is a submodule of the Activity Stream. Provides an
interface for setting the twitter account where the information will
be consumed. Currently working with the timeline tweets the user. And list
tweets the specific hash tag.

Through the administrative interface you can manage
the keys provided by Instagram API and the hash tag where the activities
will be imported.


REQUIREMENTS
------------
This module requires the following modules:
 * Activity Stream (https://drupal.org/project/activitystream).
 * Libraries API (https://drupal.org/project/libraries).
 * The PHP Library OAuth for Twitter's REST
   API (https://github.com/abraham/twitteroauth).

INSTALLATION
------------
* Install as you would normally install a contributed drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
* Download the Library OAuth for Twitter
  (https://github.com/abraham/twitteroauth). Extract the folder twitteroauth in
  "sites/all/libraries/".

CONFIGURATION
-------------
* After create your application in (https://apps.twitter.com/)
  go configure it at user/#/edit/activity-stream the following fields:
   - Twitter user name: The user name where the tweets will be consumed.
   - Twitter hashtag: The hashtag where the tweets will be consumed.
   - Twitter count: Number of streams to be imported;
   - App ID: Provided Twitter API;
   - App secret: Provided Twitter API;
   - Access Token: Provided Twitter API;
   - Access Token Secret: Provided Twitter API;

* To import the streams run cron drupal.

* View the site-wide activity stream at /activity-stream.

* View the per-user activity stream at /user/#/activity-stream.


MAINTAINERS
-----------
Current maintainers:
 * Denis Souza (denison) - https://drupal.org/user/1669786
