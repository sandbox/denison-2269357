<?php

/**
 * @file
 * Page callbacks for the Activity Stream Twitter module.
 */

/**
 * Get the twitts from the timeline of the User.
 *
 * @link https://dev.twitter.com/docs/api/1.1 @endlink
 *
 * @param array $credential
 *   An array containing the information to connect to the service.
 *    - username: The user name of twitter.
 *    - count: Specifies the number of tweets to try and retrieve.
 *    - app_id: Consumer key provided by the twitter api.
 *    - app_secret: Consumer secret provided by the twitter api.
 *    - access_token: Access token provided by the twitter api.
 *    - access_token_secret: Access token secret provided by the twitter api.
 *    - callback_url: The callback URL your site.
 *
 * @return array
 *   An array of $items, with each individual item array containing:
 *    - title: (required) The title of the activity item.
 *    - body: (required) The body of the activity item.
 *    - link: (required) A URL to the original item on the service.
 *    - timestamp: (required) A UNIX timestamp for when the activity occurred.
 *    - guid: (required) A globally unique ID for this activity item.
 *    - raw: (desired) One of the core goals behind Activity Stream is to
 *      provide an archive of your activity spread throughout the web.
 */
function _activitystream_twitter_tweets_items($credential) {

  // Request access in Oauth API.
  $con = _activitystream_twitter_connect_oauth($credential);

  if (!empty($credential['username']) && !empty($credential['hashtag'])) {

    // Prepares the url to get the tweets from inside the user hashtag.
    $tweets_data = $con->get(ACTIVITYSTREAM_TWITTER_API_URL . 'search/tweets.json', array('q' => 'from:@' . $credential['username'] . '#' . $credential['hashtag'], 'count' => $credential['count']));
  }
  elseif (!empty($credential['username'])) {

    // Prepares the url to get the tweets from time line the user.
    $tweets_data = $con->get(ACTIVITYSTREAM_TWITTER_API_URL . 'statuses/user_timeline.json', array('screen_name' => $credential['username'], 'count' => $credential['count']));
  }
  elseif (!empty($credential['hashtag'])) {

    // Prepares the url to get the tweets from hashtag.
    $tweets_data = $con->get(ACTIVITYSTREAM_TWITTER_API_URL . 'search/tweets.json', array('q' => '#' . $credential['hashtag'], 'count' => $credential['count']));
  }

  // Normalize structure data returning Twitter API.
  $tweets_data = isset($tweets_data->statuses) ? $tweets_data->statuses : $tweets_data;
  $items = array();

  foreach ($tweets_data as $tweet_item) {

    $items[] = array(
      'title'     => 'Twitter: ' . $tweet_item->id,
      'body'      => _activitystream_twitter_convert_links($tweet_item),
      'link'      => ACTIVITYSTREAM_TWITTER_STATUSES_URL . $tweet_item->id,
      'guid'      => $tweet_item->id,
      'raw'       => json_encode($tweet_item),
      'timestamp' => strtotime($tweet_item->created_at),
    );
  }

  return $items;
}

/**
 * Converts @mentions and #hashtags for hyperlinks.
 *
 * @param object $tweet_item
 *   An obaject tweet's.
 *
 * @return string
 *   The tweet's text with hyperlinks added.
 */
function _activitystream_twitter_convert_links($tweet_item) {

  $attributes = array(
    'attributes' => array(
      'target' => '_blank',
    ),
  );

  // Convert @user_mentions in links.
  if (isset($tweet_item->entities->user_mentions)) {

    foreach ($tweet_item->entities->user_mentions as $entity) {
      $link = ACTIVITYSTREAM_TWITTER_URL . $entity->screen_name;
      $screen_name = '@' . $entity->screen_name;
      $tweet_item->text = str_replace($screen_name, l($screen_name, $link, $attributes), $tweet_item->text);
    }
  }

  // Convert #hashtags in links.
  if (isset($tweet_item->entities->hashtags)) {

    foreach ($tweet_item->entities->hashtags as $entity) {
      $link = ACTIVITYSTREAM_TWITTER_URL . "search?q=%23" . $entity->text;
      $hashtag = '#' . $entity->text;
      $tweet_item->text = str_replace($hashtag, l($hashtag, $link, $attributes), $tweet_item->text);
    }
  }

  // Convert urls in links.
  if (isset($tweet_item->entities->urls)) {

    foreach ($tweet_item->entities->urls as $entity) {
      $tweet_item->text = str_replace($entity->url, l($entity->url, $entity->url, $attributes), $tweet_item->text);
    }
  }

  // Convert Media in links.
  if (isset($tweet_item->entities->media)) {

    foreach ($tweet_item->entities->media as $entity) {
      $tweet_item->text = str_replace($entity->url, l($entity->url, $entity->url, $attributes), $tweet_item->text);
    }
  }

  return _activitystream_twitter_remove_emoji($tweet_item->text);
}

/**
 * Connection twitter by Oauth.
 *
 * @param array $credential
 *   An array containing the information to connect to the service.
 *    - username: The user name of twitter.
 *    - count: Specifies the number of tweets to try and retrieve.
 *    - app_id: Consumer key provided by the twitter api.
 *    - app_secret: Consumer secret provided by the twitter api.
 *    - access_token: Access token provided by the twitter api.
 *    - access_token_secret: Access token secret provided by the twitter api.
 *    - callback_url: The callback URL your site.
 *
 * @return object
 *   An object with connection informations.
 */
function _activitystream_twitter_connect_oauth($credential) {

  if (($library = libraries_load('twitteroauth')) && !empty($library['installed'])) {

    // Request authorization.
    $connect = new TwitterOAuth(
      $credential['app_id'],
      $credential['app_secret'],
      $credential['access_token'],
      $credential['access_token_secret']
    );

    // Checks whether it was possible to connect to Twitter.
    if (isset($connect->errors)) {

      foreach ($connect->errors as $error) {

        $msg = 'Code: ' . $error->code . '=>' . 'Message: ' . $error->messaage;
        watchdog('activitystream_twitter', $msg, array(), WATCHDOG_ERROR);
      }
    }
    else {
      return $connect;
    }
  }
  else {
    drupal_set_message(t('Error loading the library <a href="@twitteroauth">twitteroauth</a>. Verify that the library is installed in the folder /all/libraries', array('@twitteroauth' => 'https://github.com/abraham/twitteroauth')), 'error');
  }
}

/**
 * Remove the emoji.
 */
function _activitystream_twitter_remove_emoji($text) {

  return preg_replace('/([0-9|#][\x{20E3}])|[\x{00ae}|\x{00a9}|\x{203C}|\x{2047}|\x{2048}|\x{2049}|\x{3030}|\x{303D}|\x{2139}|\x{2122}|\x{3297}|\x{3299}][\x{FE00}-\x{FEFF}]?|[\x{2190}-\x{21FF}][\x{FE00}-\x{FEFF}]?|[\x{2300}-\x{23FF}][\x{FE00}-\x{FEFF}]?|[\x{2460}-\x{24FF}][\x{FE00}-\x{FEFF}]?|[\x{25A0}-\x{25FF}][\x{FE00}-\x{FEFF}]?|[\x{2600}-\x{27BF}][\x{FE00}-\x{FEFF}]?|[\x{2900}-\x{297F}][\x{FE00}-\x{FEFF}]?|[\x{2B00}-\x{2BF0}][\x{FE00}-\x{FEFF}]?|[\x{1F000}-\x{1F6FF}][\x{FE00}-\x{FEFF}]?/u', '', $text);
}
